#!/bin/bash -e

fix_partuuid() {
  DISKID="$(fdisk -l "$DATA_DEV" | sed -n 's/Disk identifier: 0x\([^ ]*\)/\1/p')"

  sed -i "s/${OLD_DISKID}/${DISKID}/g" /etc/fstab
  sed -i "s/${OLD_DISKID}/${DISKID}/" /boot/cmdline.txt
}

#resize partition
DATA_PART_DEV=$(findmnt /data -o source -n)
DATA_PART_NAME=$(echo "$DATA_PART_DEV" | cut -d "/" -f 3)
DATA_DEV_NAME=$(echo /sys/block/*/"${DATA_PART_NAME}" | cut -d "/" -f 4)
DATA_DEV="/dev/${DATA_DEV_NAME}"
DATA_PART_NUM=$(cat "/sys/block/${DATA_DEV_NAME}/${DATA_PART_NAME}/partition")

OLD_DISKID=$(fdisk -l "$DATA_DEV" | sed -n 's/Disk identifier: 0x\([^ ]*\)/\1/p')

DATA_DEV_SIZE=$(cat "/sys/block/${DATA_DEV_NAME}/size")
TARGET_END=$((DATA_DEV_SIZE - 1))

umount /data #unmount for resize

if ! parted -m "$DATA_DEV" u s resizepart "$DATA_PART_NUM" "$TARGET_END"; then
	echo "Root partition resize failed" 
	return 1
fi

fix_partuuid

#resize filesystem
e2fsck -p -f $DATA_PART_DEV
resize2fs $DATA_PART_DEV
mount /data

systemctl enable --now searchwing-payloads-camera.service
systemctl enable --now searchwing-payloads-temp-humid.service
systemctl enable --now searchwing-preflight-selftest.service
systemctl enable --now searchwing-preflight-selftest.timer
systemctl enable --now searchwing-simple-webserver.service

systemctl disable resize_data.service
systemctl stop resize_data.service
