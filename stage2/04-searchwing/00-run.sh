#!/bin/bash -e

#boot partition => enable cam/serialport
install -m 644 files/cmdline.txt "${ROOTFS_DIR}/boot/"
install -m 644 files/config.txt "${ROOTFS_DIR}/boot/"

#set static ip for wifi
#echo " " >> ${ROOTFS_DIR}/etc/dhcpcd.conf
#echo "#set static ip for wifi" >> ${ROOTFS_DIR}/etc/dhcpcd.conf
#echo "interface wlan0" >> ${ROOTFS_DIR}/etc/dhcpcd.conf
#echo "static ip_address=${STATIC_WIFI_IP}" >> ${ROOTFS_DIR}/etc/dhcpcd.conf
#echo "static routers=${WIFI_GATEWAY}" >> ${ROOTFS_DIR}/etc/dhcpcd.conf
#echo "static domain_name_servers=${WIFI_DNS}" >> ${ROOTFS_DIR}/etc/dhcpcd.conf

# Disable unused services
# hciuart => Bluetooth service
# rsyslog => Not used
# systemd-timesyncd => timesync not needed
on_chroot << EOF
sudo systemctl disable hciuart
sudo systemctl disable rsyslog
sudo systemctl disable systemd-timesyncd
EOF

#setup vsftp for ftp remote access with 'searchwing' user
install -m 644 files/vsftpd.conf "${ROOTFS_DIR}/etc/"
install -m 644 files/vsftpd.chroot_list "${ROOTFS_DIR}/etc/"

# Add example files for headless configuration
install -m 644 files/ssh "${ROOTFS_DIR}/boot/"
install -m 644 files/wpa_supplicant.conf "${ROOTFS_DIR}/boot/"

#setup rsync for image to groundstation data transfer
install -m 644 files/rsyncd.conf "${ROOTFS_DIR}/etc/"
on_chroot << EOF
sudo systemctl enable rsync
EOF

# Add custom message of the day for ssh login
install -m 644 files/motd "${ROOTFS_DIR}/etc/"

# Add script for custom headless configuration
install -m 644 files/unattended "${ROOTFS_DIR}/boot/"
