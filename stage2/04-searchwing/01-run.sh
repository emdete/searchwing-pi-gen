#!/bin/bash -e

#install searchwing-pi
rm -rf ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/searchwing-pi
mkdir -p ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/searchwing-pi
git clone --depth 1 -b master https://searchwing-robot:fiVQQM8JF5y4bxVtJyRx@gitlab.com/searchwing/development/searchwing-pi ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/searchwing-pi
mkdir -p ${ROOTFS_DIR}/data/bilder
mkdir -p ${ROOTFS_DIR}/data/logs

on_chroot << EOF
chown -R ${FIRST_USER_NAME}:${FIRST_USER_NAME} /home/${FIRST_USER_NAME}/
EOF

# Install python packages for searchwing-pi based on the provided requirements file
on_chroot << EOF
pip3 install -r /home/${FIRST_USER_NAME}/searchwing-pi/requirements.txt
EOF

#compile/install mavlink-router
on_chroot << EOF
./home/${FIRST_USER_NAME}/searchwing-pi/scripts/install_mavlinkRouter.sh
EOF
mkdir -p ${ROOTFS_DIR}/etc/mavlink-router/config.d
install -m 644 ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/searchwing-pi/root/etc/mavlink-router/config.d/main.conf "${ROOTFS_DIR}/etc/mavlink-router/config.d/"


#install searchwing-pi services
install -m 644 ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/searchwing-pi/root/etc/systemd/system/* "${ROOTFS_DIR}/etc/systemd/system/" 
#services will be enabled after home partition got resized


#resize home partition as service for first boot only
install -m 744 files/resize_data.sh "${ROOTFS_DIR}/"
install -m 744 files/resize_data.service "${ROOTFS_DIR}/etc/systemd/system/"
on_chroot << EOF
systemctl enable resize_data
EOF

#add cronjob to set fake-hwclock every minute (instead of every hour) to save current clock for reboot
on_chroot << EOF
sudo echo "*  *    * * *   root    /etc/cron.hourly/fake-hwclock" | sudo tee --append /etc/crontab
EOF

#add passwordless sudo
on_chroot << EOF
sudo echo "${FIRST_USER_NAME} ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers
EOF

#added auto reboot in case of kernel panic / system freeze
on_chroot << EOF
sudo echo "kernel.panic = 0" >> /etc/sysctl.conf
EOF

##disable hdmi
on_chroot << EOF
sudo echo "/opt/vc/bin/tvservice -o" >> /etc/rc.local
sudo echo "/usr/bin/tvservice -o" >> /etc/rc.local
EOF

#install groundstation ssh pub key
mkdir -p ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/.ssh
install -m 744 sshkeys/id_rsa.pub "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/.ssh/"
