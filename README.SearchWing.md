# searchwing-pi-gen

This repo creates a raspbian lite image with all necessary settings and scripts for the searchwing drone.

Created images can be found here:

https://cloud.hs-augsburg.de/s/6cia2g5xpoCjjZX

Features:
* Dynamic IP from Accesspoint
* Predefined User / Password (set to searchwing / cassandra)
* Enabled SSH-Server
* Datatransfer via RSYNC (enabled) / FTP-Server (disabled by default)
* Additional /data partition for camera images
* Automatic resize of /data partition to sd card size on first boot
* Enabled camera connector
* Enabled serial connection (RX @ Pin10) 
* Mavproxy installed for dataflash log download (just in case...)
* Includes and enables searchwing-pi repo features (clones the repo into image)
	* searchwing-payloads-camera.service: Taking camera images with GPS stamps via pixhawk connection 
	* searchwing-payloads-temp-humid.service: Logging of core temperature, box temperature and humidity
	* searchwing-simple-webserver.service: Simple webserver to check camera images
* Ethernet Gadget @ pi zero USB port: If connected to a laptop, you can login into the pizero via ssh (see section below)

Checkout the `config` file for all predefined values.

TODO

* [X] create additional partition for images
* [X] testing in simulation
* [X] testing on flight 
* [ ] make root partition read-only for protection (may not be necessary)

## Image creation

### Prerequisites:

- The image build creates around 60GB of output
- Docker installed
- Internet connection
- Current user added to group 'docker', else you must use sudo for `docker run`

### How to...

#### Add new Python Packages to the image

Edit [stage2/04-searchwing/01-run.sh](./stage2/04-searchwing/01-run.sh).

#### Update from Original Repo

The original pi-gen repo can be found on [GitHub](https://github.com/RPi-Distro/pi-gen.git). In order to fetch updates
from the repo simply add it as an additional remote and fetch.

### Build Image 

Run image creation by using

`PRESERVE_CONTAINER=1 ./build-docker.sh -c config`

The image will be created in the `deploy` folder. 

Most of the features are implemented in `stage02/04-searchwing`. The paritioning is implemented in `export-image`.

## post image creation setup

Described in https://www.hs-augsburg.de/homes/beckmanf/dokuwiki/doku.php?id=flash_prepare_pi_zero_image

## edit image after creation

* unzip image
* get offsets: `sudo parted 2020-02-20-searchwing-pi-lite.img unit s print`
* mount second partition (rootfs) by using its start and size: `sudo mount 2020-02-20-searchwing-pi-lite.img /mnt/point -o loop,offset=$((512*start)),sizelimit=$((512*Size))`
* unmount and rezip

## Login into pi zero via USB gadget

* connect Pi zero USB port via Micro USB to Laptop
* Find link local address: `ping ff02::1%enp7s0f3u2` where `enp7s0f3u2` is the local ethernet device on your laptop. The link local address is usually the second ping response (or the IP which does more slowly answer than the other).
* Login via SSH: `ssh searchwing@fe80::f405:430a:9dc7:db7b%enp7s0f3u2` where `fe80::f405:430a:9dc7:db7b%enp7s0f3u2`is the found link ip.